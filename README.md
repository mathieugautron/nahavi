**Déploiement et lancement**


**Dépendances**

Pour lancer `rabbitMQ`, il vous faudra une installation locale, ou bien simplement avoir `docker` installé.

Pour compiler et générer les jars, il vous faudra `Maven`.

Pour utiliser notre script de démonstration il vous faudra `Maven`, un service rabbitMQ en exécution, 
et enfin avoir xterm installé pour l'ouverture de fenêtres d'intératctions.

**Lancement de l'application**
* Lancer rabbitmq.
 
    Par exemple avec la commande suivante : 
```bash
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management
```
* Lancer des noeuds et des clients:
```bash
./nahavi-run.sh [nombre de noeud serveur] [nombre de clients]
```
* Ajouter des clients à volonté :
```bash
java -jar taget/client.jar
```

* Pour exécuter sans passer par le script mais par les jar, générez les jars avec :
```bash
mvn package
```

