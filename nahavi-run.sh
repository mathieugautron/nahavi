#!/bin/bash
if [ $# -ne 2 ]
then
  echo "Usage : ./nahavi-run [nombre de noeud serveur] [nombre de clients]"
  echo "Nécessite que Maven et xterm soient installés."
  exit
fi

#apt-get update && apt-get install -y maven xterm
mvn package

timer=5
cd target/
counter=1
while [ $counter -le $1 ]
do
  xterm -title "Node" -e "java -jar node.jar $timer" &
  sleep 1
  ((counter++))
done

sleep $timer

counter=1
while [ $counter -le $2 ]
do
  xterm -title "client" -e "java -jar client.jar" &
  sleep 1
  #pid=$!
  ((counter++))
done

#wait $pid
