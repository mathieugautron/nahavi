package server;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Timeout extends Thread {
    private PropertyChangeListener pcl;
    private Timer timer;
    private final Integer secondsToWait;
    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");

    // Constructeur
    public Timeout(PropertyChangeListener pcl, Integer secondsToWait){
        this.secondsToWait = secondsToWait;
        this.pcl = pcl;
        timer = new Timer("Timer");
    }

    public void run() {
        System.out.println("--- Début du timer : " + sdf.format(new Date()) );
        startCounting();
    }

    public void stopTimer(){
        timer.cancel();
    }

    public void refreshTimer(){
        timer.cancel();
        timer = new Timer("timer");
        System.out.println("--- Timer refresh : " + sdf.format(new Date()) );
        startCounting();
    }


    private void startCounting() {
        TimerTask task = new TimerTask() {
            public void run() {
                System.out.println("--- Fin du timer : "  + sdf.format(new Date()) );
                pcl.propertyChange(new PropertyChangeEvent(this,"fin",null,null));
            }
        };
        long delay = 1000L;
        timer.schedule(task, secondsToWait * delay);
    }

}
