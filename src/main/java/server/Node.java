package server;

import com.rabbitmq.client.*;

import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Node {
    private static final int MEMORY_SIZE = 100;
    private final Integer [] memory;
    private final Boolean [] allocationBoolMap;
    private final UUID id;
    private Channel channel;
    private String nodeRequestQueue;
    private String nodeResponseQueue;
    private String starAckQueue;
    private String clientRequestQueue;
    private String clientConnectionQueue;
    private final String initialisationQueue;
    private Boolean hasInitFinished;
    private Boolean isFirstNode;
    private final List<UUID> knownNodes;
    private final List<UUID> clients;
    private final Timeout timeout;
    private final Condition startStarAckSignal;
    private final Condition startServiceSignal;
    private final Lock lock;
    private Boolean isStarted;
    private Boolean isWaitingForChargeResponse;
    private Integer nbStartAck;
    private final Map<UUID, Integer> currentCharges;
    private List<UUID> currentClientConnection;
    private String firstNodeID;
    private final List<Variable> localVariables;
    private final StringBuilder allVariables;

    private static final String INTERNAL_EXCHANGE_NAME = "node_director";
    private static final String EXTERNAL_EXCHANGE_NAME = "client_director";


    /* *********************************** */
    /*           Constructeur(s)           */
    /* *********************************** */


    public Node(int initialisationPhaseTimeoutDuration) throws Exception {
        id = UUID.randomUUID();
        nbStartAck = 0;
        isStarted = false;
        isFirstNode = true;
        hasInitFinished = false;
        isWaitingForChargeResponse = false;

        allVariables = new StringBuilder();
        clientConnectionQueue = "clientConnection";
        clientRequestQueue = "clientRequest@"+id.toString();
        initialisationQueue = "init@"+id.toString();

        currentClientConnection = new ArrayList<>();
        currentCharges = new HashMap<>();
        knownNodes = new LinkedList<>();
        memory = new Integer[MEMORY_SIZE];
        clients = new ArrayList<>();
        localVariables = new ArrayList<>();
        allocationBoolMap = new Boolean[MEMORY_SIZE];
        Arrays.fill(allocationBoolMap, Boolean.FALSE);

        lock = new ReentrantLock();
        startServiceSignal = lock.newCondition();
        startStarAckSignal = lock.newCondition();

        System.out.println("Je suis le noeud : "+ id.toString());

        PropertyChangeListener pcl = propertyChangeEvent -> {
            System.out.println("Début du protocole starAck");
            if(isFirstNode){
                String message = "Start@"+id.toString();
                try {
                    channel.basicPublish(INTERNAL_EXCHANGE_NAME, "starAck", null, message.getBytes(StandardCharsets.UTF_8));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        //Integer initialisationPhaseTimeoutDuration = timer;
        timeout = new Timeout(pcl, initialisationPhaseTimeoutDuration);

        System.out.println("Début de la phase d'initialisation");
        initConnection();
        waitForStartStarAck();
        startStarAckProtocol(firstNodeID);
        waitForStartService();
        initService();
    }


    /* *********************************** */
    /*    Phase 1: connexion des nodes    */
    /* *********************************** */


    private void initConnection() throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        channel = connection.createChannel();
        channel.exchangeDeclare(INTERNAL_EXCHANGE_NAME, "direct");
        initInitialisationQueues();
        channel.basicPublish(INTERNAL_EXCHANGE_NAME, "initialisation", null, id.toString().getBytes(StandardCharsets.UTF_8));
        timeout.start();
    }

    private void initInitialisationQueues() throws IOException {
        nodeRequestQueue = "request@"+id.toString();
        nodeResponseQueue = "response@"+id.toString();
        starAckQueue = "starAck@"+id.toString();

        channel.queueDeclare(nodeRequestQueue,false,false,false,null);
        channel.queueDeclare(nodeResponseQueue,false,false,false,null);
        channel.queueDeclare(starAckQueue,false,false,false,null);
        channel.queueDeclare(initialisationQueue, false, false, false, null);
        handleInitialisationMessages();
        handleNodeResponse();
        handleStarAckMessages();
    }



    private void handleInitialisationMessages() throws IOException {
        channel.queueBind(initialisationQueue, INTERNAL_EXCHANGE_NAME, "initialisation");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String nodeID = new String(delivery.getBody(), StandardCharsets.UTF_8);
            if( !nodeID.equals(id.toString()) ){
                System.out.println("Un noeud vient d'arriver : "+nodeID);
                knownNodes.add(UUID.fromString(nodeID));
                timeout.refreshTimer();
                channel.basicPublish("", "response@"+nodeID, null, id.toString().getBytes(StandardCharsets.UTF_8));
            }
        };
        channel.basicConsume(initialisationQueue, true, deliverCallback, consumerTag -> { });

    }

    private void handleNodeResponse() throws IOException {
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            isFirstNode = false;
            String nodeID = new String(delivery.getBody(), StandardCharsets.UTF_8);
            if ( !knownNodes.contains(UUID.fromString(nodeID)) ){
                knownNodes.add(UUID.fromString(nodeID));
            }
        };
        channel.basicConsume(nodeResponseQueue, true, deliverCallback, consumerTag -> { });
    }


    /* *********************************** */
    /*     Phase 2: protocole starAck      */
    /* *********************************** */


    private void handleStarAckMessages() throws IOException {
        channel.queueBind(starAckQueue, INTERNAL_EXCHANGE_NAME, "starAck");
        nbStartAck = 0;

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            String [] messageInfos = message.split("@");
            switch (messageInfos[0]){
                case "Start":
                    this.firstNodeID = messageInfos[1];
                    startStarAck();
                    break;

                case "StartAck":
                    if (isFirstNode){
                        nbStartAck++;
                        if( canWeStart() ){
                            notifyNodesThatWeCanStart();
                        }
                    }
                    break;

                case "Go":
                    System.out.println("Fin de protocole starAck, les noeuds sont initialisés");
                    startService();
                    cleanInitialisationPhaseObject();
                    break;
            }
        };
        channel.basicConsume(starAckQueue, true, deliverCallback, consumerTag -> { });
    }

    private void notifyNodesThatWeCanStart() throws IOException {
        String launchingMessage = "Go@"+id.toString();
        for(UUID nodeID : knownNodes){
            channel.basicPublish("", "starAck@"+nodeID, null, launchingMessage.getBytes(StandardCharsets.UTF_8));
        }
        channel.basicPublish("", "starAck@"+id.toString(), null, launchingMessage.getBytes(StandardCharsets.UTF_8));
    }

    private Boolean canWeStart(){
        return nbStartAck == knownNodes.size() + 1; /* On ajoute un pour la réponse du noeud lui même */
    }

    private void waitForStartStarAck(){
        lock.lock();
        try {
            while (!hasInitFinished) {
                startStarAckSignal.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    private void startStarAck(){
        lock.lock();
        try {
            hasInitFinished = true;
            startStarAckSignal.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private void waitForStartService(){
        lock.lock();
        try {
            while (!isStarted) {
                startServiceSignal.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
    }

    private void startService(){
        lock.lock();
        try {
            isStarted = true;
            startServiceSignal.signalAll();
        } finally {
            lock.unlock();
        }
    }




    private void startStarAckProtocol(String firstNodeId) throws IOException {
        channel.queueUnbind(nodeRequestQueue,INTERNAL_EXCHANGE_NAME,"initialisation");
        System.out.println("Start reçu et startAck envoyé");
        String message = "StartAck@"+id.toString();
        channel.basicPublish("", "starAck@"+firstNodeId, null, message.getBytes(StandardCharsets.UTF_8));
    }

    private void cleanInitialisationPhaseObject() throws IOException {
        timeout.stopTimer();
        channel.queuePurge(starAckQueue);
        channel.queueUnbind(starAckQueue,INTERNAL_EXCHANGE_NAME,"starAck");
        channel.queueDelete(nodeResponseQueue);
        channel.queueDelete(starAckQueue);
    }


    /* ********************************************** */
    /* Phase 3.1: Mise en place des services internes */
    /* ********************************************** */

    private void initService() throws IOException {
        assert isStarted;
        System.out.println("Initialisation de la prise en charge des clients");
        initInternalService();
        initExternalService();
        System.out.println("Début de la prise en charge des clients");
        System.out.println("Le serveur est initialisé.");
    }

    private void initInternalService() throws IOException {
        assert isStarted;
        initRequestQueueService();

    }

    private void initRequestQueueService() throws IOException {
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String receivedPacketString = new String(delivery.getBody(), StandardCharsets.UTF_8);
            Packet receivedPacket = new Packet(receivedPacketString);
            /* On s'assure que le packet recu ne sois pas corrompu. */
            if( !receivedPacket.isValid(knownNodes,id,MEMORY_SIZE) ){
                Packet errorPacket = receivedPacket.getCreationErrorPacket(id);
                if( !errorPacket.error_type.equals(SERVER_ERROR.NOT_AN_UUID) ){ /* Si le problème du packet n'est pas son champ 'Sender' */
                    /* Retour à l'envoyeur */
                    channel.basicPublish("", "request@"+receivedPacket.sender, null, errorPacket.toString().getBytes(StandardCharsets.UTF_8));
                }
            }

            /* Si le paquet ne m'est pas destiné, on le forward au bon noeud, qui répondra au client. */
            if( !receivedPacket.variable.getAddress().getNodeId().equals(id) ){
                channel.basicPublish("", "request@"+receivedPacket.variable.getAddress().getNodeId(), null, receivedPacket.toString().getBytes(StandardCharsets.UTF_8));
                return;
            }

            /* A ce stade, on est sur que le packet est bien formé, et qu'il nous ait adressé. */
            switch (receivedPacket.packet_type){
                case ASKCHARGE:
                    handleAskChargeMessage(receivedPacket);
                    break;

                case GIVECHARGE:
                    handleGiveChargeMessage(receivedPacket);
                    break;

                case NEWCLIENT:
                    handleNewClientMessage(receivedPacket);
                    break;
            }
        };
        System.out.println("Les services internes du noeud sont prêts");
        channel.basicConsume(nodeRequestQueue, true, deliverCallback, consumerTag -> {
        });
    }

    private void handleNewClientMessage(Packet receivedPacket){
        String [] receivedClients = receivedPacket.data.split(":");
        for( String client : receivedClients){
            clients.add(UUID.fromString(client));
        }
    }


    private void handleGiveChargeMessage(Packet receivedPacket) throws IOException {
        String[] infos = receivedPacket.data.split(":");
        currentCharges.put(receivedPacket.sender, Integer.parseInt(infos[0]));
        if(infos.length == 2) {
            allVariables.append( infos[1] );
        }
        /* Quand on a reçu toutes les réponses de charges on determine qui s'occupera du nouveau client */
        if( currentCharges.size() == knownNodes.size() ) {
            isWaitingForChargeResponse = false;
            int minimalCharge = clients.size();
            UUID selectedNode = id; /* Juste pour initialiser, va etre obligatoirement set plus tard */
            for(UUID nodeId : currentCharges.keySet()) {
                Integer currentNodeCharge = currentCharges.get(nodeId);
                if(minimalCharge > currentNodeCharge) {
                    minimalCharge = currentNodeCharge;
                    selectedNode = nodeId;
                }
            }
            /* On envoie maintenant le nom de la queue du noeud désigné au clients
             qui se connectent et la liste des variables existantes */
            String requestQueueSelected = "clientRequest@"+selectedNode.toString();
            for(Variable v : localVariables) {
                allVariables.append(v);
                allVariables.append("/");
            }
            String data = requestQueueSelected+":"+ allVariables.toString();
            Packet responseForClient = new Packet(id, new Variable("", new Address(receivedPacket.sender, 0), 0), PACKET_TYPE.SETCONNECTION, false, data);
            StringBuilder dataToNode = new StringBuilder();
            for( UUID clientID : currentClientConnection){
                channel.basicPublish("", "clientResponse@"+clientID.toString(), null, responseForClient.toString().getBytes(StandardCharsets.UTF_8));
                dataToNode.append(clientID);
                dataToNode.append(":");
            }

            // et on envoi au noeud en charge les ID de ses nouveaux clients
            Packet newClientPacket = new Packet(id,new Variable("",new Address(selectedNode,0),1),PACKET_TYPE.NEWCLIENT,false,dataToNode.toString(),UUID.randomUUID() );
            channel.basicPublish("", "request@"+selectedNode.toString(), null, newClientPacket.toString().getBytes(StandardCharsets.UTF_8));
            currentClientConnection = new ArrayList<>();
        }
    }

    private void handleAskChargeMessage(Packet receivedPacket) throws IOException {
        /* On envoie notre charge actuelle en terme de client et nos variables locales */
        StringBuilder data = new StringBuilder( String.valueOf(clients.size()) );
        data.append(clients.size());
        data.append(":");
        for(Variable var : localVariables){
            data.append(var);
            data.append("/");
        }
        Packet charge = new Packet(id, new Variable("", new Address(receivedPacket.sender, 0), 0), PACKET_TYPE.GIVECHARGE, false, data.toString());
        channel.basicPublish("", "request@"+receivedPacket.sender, null, charge.toString().getBytes(StandardCharsets.UTF_8));
    }



    /* ********************************************** */
    /* Phase 3.2: Mise en place des services externes */
    /* ********************************************** */


    private void initExternalService() throws IOException {
        initClientConnection();
        initClientRequest();
    }

    private void initClientConnection() throws IOException {
        channel.exchangeDeclare(EXTERNAL_EXCHANGE_NAME, "direct");
        clientConnectionQueue = "clientConnection";
        channel.queueDeclare(clientConnectionQueue, false, false, false, null);
        channel.queueBind(clientConnectionQueue, EXTERNAL_EXCHANGE_NAME, "connection");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            /* Preparation du paquet demandant la charge d'un node */
            String clientID = new String(delivery.getBody(), StandardCharsets.UTF_8);
            System.out.println("Un client essaie de se connecter : " + clientID);
            currentClientConnection.add( UUID.fromString(clientID) );
            if(!isWaitingForChargeResponse){
                /* On prend en compte le cas ou il n'y a qu'un seul noeud */
                if(knownNodes.size() == 0) {
                    for(Variable v : localVariables) {
                        allVariables.append(v);
                        allVariables.append("/");
                    }
                    clients.add(UUID.fromString(clientID));
                    String data = clientRequestQueue+":"+ allVariables.toString();
                    Packet responseForClient = new Packet(id, new Variable("", new Address(UUID.fromString(clientID), 0), 0), PACKET_TYPE.SETCONNECTION, false, data);
                    channel.basicPublish("", "clientResponse@"+ clientID, null, responseForClient.toString().getBytes(StandardCharsets.UTF_8));
                } else {
                    /* Sinon => Broadcast de la demande de charge à tous les nodes */
                    Packet chargeRequest = new Packet(id, null, PACKET_TYPE.ASKCHARGE, false);
                    for(UUID nodeId : knownNodes) {
                        chargeRequest.variable = new Variable("",new Address(nodeId,0), 0);
                        channel.basicPublish("", "request@"+nodeId.toString(), null, chargeRequest.toString().getBytes(StandardCharsets.UTF_8));
                    }
                    isWaitingForChargeResponse = true;
                }
            }

        };
        channel.basicConsume(clientConnectionQueue, true, deliverCallback, consumerTag -> { });
    }

    private void initClientRequest() throws IOException {
        clientRequestQueue = "clientRequest@" + id.toString();
        channel.queueDeclare(clientRequestQueue, false, false, false, null);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String receivedPacketString = new String(delivery.getBody(), StandardCharsets.UTF_8);
            Packet receivedPacket = new Packet(receivedPacketString);

            /* On s'assure que le packet recu ne sois pas corrompu. */
            if( !receivedPacket.isValid(knownNodes,id,MEMORY_SIZE) ){
                Packet errorPacket = receivedPacket.getCreationErrorPacket(id);
                if( !errorPacket.error_type.equals(SERVER_ERROR.NOT_AN_UUID) ){ /* Si le problème du packet n'est pas son champ 'Sender' */
                    /* Retour à l'envoyeur */
                    channel.basicPublish("", "response@"+receivedPacket.sender, null, errorPacket.toString().getBytes(StandardCharsets.UTF_8));
                }
            }

            /* Si le paquet ne m'est pas destiné, on le forward au bon noeud, qui répondra au client.*/
            if( !receivedPacket.variable.getAddress().getNodeId().equals(id) && receivedPacket.packet_type != PACKET_TYPE.ALLOCATE && receivedPacket.packet_type != PACKET_TYPE.CLIENTDISCONNECTION ){
                channel.basicPublish("", "clientRequest@"+receivedPacket.variable.getAddress().getNodeId(), null, receivedPacket.toString().getBytes(StandardCharsets.UTF_8));
                return;
            }

            /* A ce stade, on est sur que le packet est bien formé, et qu'il nous ait adressé. */
            switch (receivedPacket.packet_type){
                case READ:
                    System.out.println("Le client "+receivedPacket.sender+" veut lire la variable "+receivedPacket.variable.getName() );
                    handleReadRequests(receivedPacket);
                    break;

                case WRITE:
                    System.out.println("Le client "+receivedPacket.sender+" veut écrire sur variable "+receivedPacket.variable.getName() );
                    handleWriteRequests(receivedPacket);
                    break;

                case ALLOCATE:
                    System.out.println("Le client "+receivedPacket.sender+" veut allouer une variable nommée : "+receivedPacket.variable.getName() );
                    handleAllocationRequest(receivedPacket);
                    break;

                case FREE:
                    System.out.println("Le client "+receivedPacket.sender+" veut libérer la variable : "+receivedPacket.variable.getName() );
                    handleFreeRequests(receivedPacket);
                    break;

                case CLIENTDISCONNECTION:
                    System.out.println("Le client "+receivedPacket.sender+" s'est déconnecté" );
                    handleClientDisconnectionMessage(receivedPacket);
                    break;

                default:
                    System.err.println("Type de requête non reconnu.");
                    break;
            }
        };
        System.out.println("Les services externes du noeud sont prêts");
        channel.basicConsume(clientRequestQueue, true, deliverCallback, consumerTag -> {
        });
    }

    private void handleClientDisconnectionMessage(Packet receivedPacket){
        clients.remove(receivedPacket.sender);
    }


    private void handleReadRequests(Packet receivedPacket) throws IOException {
        /* Si l'adresse demandée n'est pas allouée. */
        if( !allocationBoolMap[receivedPacket.variable.getAddress().getOffset()] ){
            answerAddressNotAllocated(receivedPacket);
            System.err.println("Adresse non allouée demandée par le packet suivant :");
            System.err.println(receivedPacket.toString());
            /* Sinon, cas normal, on retourne la valeur demandé. */
        }else{
            Packet responsePacket = receivedPacket.getResponsePacket(id, memory[receivedPacket.variable.getAddress().getOffset()].toString() );
            channel.basicPublish("", "clientResponse@"+receivedPacket.sender, null, responsePacket.toString().getBytes(StandardCharsets.UTF_8));
        }
    }

    private void handleWriteRequests(Packet receivedPacket) throws IOException {
        /* Si l'adresse demandée n'est pas allouée. */
        if( !allocationBoolMap[receivedPacket.variable.getAddress().getOffset()] ){
            answerAddressNotAllocated(receivedPacket);
            System.err.println("Adresse non allouée demandée par le packet suivant :");
            System.err.println(receivedPacket.toString());
            /* Sinon, cas normal, on écrit la valeur demandé. */
        }else{
            Integer valueToStore = new Integer(receivedPacket.data);
            memory[receivedPacket.variable.getAddress().getOffset()] = valueToStore;
            Packet successPacket = receivedPacket.getResponsePacket(id,"true");
            channel.basicPublish("", "clientResponse@"+receivedPacket.sender, null, successPacket.toString().getBytes(StandardCharsets.UTF_8));
        }
    }

    private void handleFreeRequests(Packet receivedPacket) throws IOException {
        int sizeToFree = receivedPacket.variable.getSize();
        for(int i=receivedPacket.variable.getAddress().getOffset(); i<sizeToFree; i++){
            allocationBoolMap[i] = false;
        }
        Packet successPacket = receivedPacket.getResponsePacket(id,"true");
        channel.basicPublish("", "clientResponse@"+receivedPacket.sender, null, successPacket.toString().getBytes(StandardCharsets.UTF_8));
        broadCastToClients(successPacket);
    }


    private void handleAllocationRequest(Packet receivedPacket) throws IOException {
        Boolean hasAllocationSucceed = false;
        Integer firstIndex = 0;
        /* On récupère la taille de l'espace à chercher dans le packet reçu.
           Pour une allocation : data = [UUID d'un noeud ayant cherché mais n'ayant pas trouvé]#["]#... */
        Integer spaceToFind = receivedPacket.variable.getSize();
        while (!hasAllocationSucceed && firstIndex != -1){
            firstIndex = findContinuousSpace(spaceToFind);
            if(firstIndex != -1){
                hasAllocationSucceed = allocateSpace(firstIndex,spaceToFind);
            }
        }

        /* Si on a pa pu trouver de segment de mémoire assez grand dans ce noeud,
           On passe la demande d'allocation a un autre noeud. */
        if( !hasAllocationSucceed ){
            forwardAllocationRequest(receivedPacket);
        }else {
            /* Si on a alloué l'espace avec succès, on peut renvoyer l'adresse de cet espace au client. */
            Address adr = new Address(id,firstIndex);
            Packet responsePacket = receivedPacket.getResponsePacket(id,"true");
            Variable newVar = new Variable(receivedPacket.variable.getName(), adr, receivedPacket.variable.getSize());
            localVariables.add(newVar);
            responsePacket.variable = newVar;
            broadCastToClients(responsePacket);
        }

    }

    private void forwardAllocationRequest(Packet receivedPacket) throws IOException {
        String[] allocationInfos = receivedPacket.data.split("#");
        boolean hasFoundNode = false;
        UUID node = id;

        if( knownNodes.size() > 0 && allocationInfos.length < (knownNodes.size()+1)  ) {

            /* On récupère tous les noeuds n'ayant pas réussis à trouver cet espace, pour ne pas leur redemander. */
            List<UUID> nodeWhoHasFailed = new LinkedList<>();
            for (String allocationInfo : allocationInfos) {
                if( !allocationInfo.isEmpty() ){
                    nodeWhoHasFailed.add(UUID.fromString(allocationInfo));
                }
            }

            /* On cherche un noeud qui n'ai pas déjà essayer d'allouer cet espace */
            int i = 0;
            while (!hasFoundNode && i < knownNodes.size()) {
                node = knownNodes.get(i);
                if (!nodeWhoHasFailed.contains(node)) {
                    hasFoundNode = true;
                }
                i++;
            }
        }

        if(hasFoundNode){
            String newData = receivedPacket.data+"#"+id.toString();
            receivedPacket.setData(newData);
            channel.basicPublish("", "clientRequest@"+node.toString(), null, receivedPacket.toString().getBytes(StandardCharsets.UTF_8));
        }else {
            Packet errorPacket = receivedPacket.getCouldntFindEnoughSpaceErrorPacket(id);
            channel.basicPublish("", "clientResponse@"+receivedPacket.sender, null, errorPacket.toString().getBytes(StandardCharsets.UTF_8));
        }

    }

    private Integer findContinuousSpace(Integer spaceToFind){
        int startingIndex = -1;
        Integer nbContinuousFreeSlots = 0;
        int i = 0;

        /* On parcours a boolMap en comptant le nombre d'espaces libres à la suite trouvé */
        while ( nbContinuousFreeSlots < spaceToFind && i < MEMORY_SIZE){
            if( allocationBoolMap[i] ) {
                nbContinuousFreeSlots = 0;
            }else{
                nbContinuousFreeSlots++;
            }
            i++;
        }

        /* Si on a trouvé un espace assez grand, alors on renvoi le premier indice de cette zone. */
        if(nbContinuousFreeSlots.equals(spaceToFind)){
            startingIndex = i - spaceToFind ;
        }

        /* Si aucun espace n'a pu être trouvé, on retourne -1 */
        return startingIndex;
    }

    private Boolean allocateSpace(Integer firstIndex, Integer sizeToAllocate){
        boolean isSuccess = true;
        Integer i = firstIndex;
        while (isSuccess && i < sizeToAllocate){
            if(!allocationBoolMap[i]){
                allocationBoolMap[i] = true;
                memory[i] = 0;
            }else{
                isSuccess = false;
            }
            i++;
        }
        return isSuccess;
    }


    private void answerAddressNotAllocated(Packet packet) throws IOException {
        Packet errorPacket = packet.getNonAllocatedAdrErrorPacket(id);
        channel.basicPublish("", "response@"+packet.sender, null, errorPacket.toString().getBytes(StandardCharsets.UTF_8));
    }

    private void broadCastToClients(Packet packet) throws IOException {
        channel.basicPublish(EXTERNAL_EXCHANGE_NAME, "client", null, packet.toString().getBytes(StandardCharsets.UTF_8));
    }




    /* ********************************************** */
    /*                     server.Main                       */
    /* ********************************************** */


    public static void main(String[] args) throws Exception {
        new Node(Integer.parseInt(args[0]));
    }
}
