package server;

import java.util.UUID;

public class Variable {
    private String name;
    private Address address;
    private Integer size;

    public Variable(String name, Address address, Integer size) {
        this.name=name;
        this.address=address;
        this.size = size;
    }

    public Variable(String variable) {
        String [] variableInfos = variable.split("@");
        this.address = new Address(UUID.fromString(variableInfos[1]), Integer.parseInt(variableInfos[0]));
        this.name = variableInfos[2];
        this.size = Integer.parseInt(variableInfos[3]);
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public Integer getSize() {
        return size;
    }

    public Variable atIndex(int index){
        if(index < size){
            return new Variable(this.name,new Address(address.getNodeId(),address.getOffset()+index),size-index);
        }else{
            throw new IndexOutOfBoundsException("Vous essayez d'accèder à une partie hors de la variable");
        }
    }

    @Override
    public String toString() {
        return address + "@" + name + "@" + size;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Variable)) return false;
        Variable cast = (Variable)o;
        return cast.name.equals(name) && cast.address.equals(address) && cast.size.equals(size);
    }
}
