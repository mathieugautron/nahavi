package server;

public enum PACKET_TYPE {
    READ ("READ"),
    WRITE("WRITE"),
    ALLOCATE("ALLOCATE"),
    FREE ("FREE"),
    ASKCHARGE("ASKCHARGE"),
    GIVECHARGE("GIVECHARGE"),
    SETCONNECTION("SETCONNECTION"),
    NEWCLIENT("NEWCLIENT"),
    CLIENTDISCONNECTION("CLIENTDISCONNECTION");


    private String name;

    PACKET_TYPE(String name){
        this.name = name;
    }

    public String toString(){
        return name;
    }
}
