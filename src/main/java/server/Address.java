package server;

import java.util.UUID;

public class Address {
    private final UUID nodeId;
    private final Integer offset;

    public Address(UUID nodeId, Integer offset) {
        this.nodeId = nodeId;
        this.offset = offset;
    }

    @Override
    public String toString() {
        return offset + "@" +nodeId;
    }

    public UUID getNodeId() {
        return nodeId;
    }

    public Integer getOffset() {
        return offset;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof Address)) return false;
        Address cast = (Address)o;
        return nodeId.equals(cast.nodeId) && offset.equals(cast.offset);
    }
}
