package server;

import java.util.List;
import java.util.UUID;

public class Packet {
    public PACKET_TYPE packet_type;
    public UUID sender;
    public Variable variable;
    public Boolean isErrorFlag;
    public String data;
    private Boolean isValid;
    public UUID packetNumber;
    public SERVER_ERROR error_type;

    public Packet(UUID sender, Variable variable, PACKET_TYPE packet_type, Boolean isErrorFlag, String data, UUID packetNumber) {
        this.packet_type = packet_type;
        this.sender = sender;
        this.variable = variable;
        this.data = data;
        this.isErrorFlag = isErrorFlag;
        this.packetNumber = packetNumber;
        isValid = true;
    }

    public Packet(UUID sender, Variable variable, PACKET_TYPE packet_type, Boolean isErrorFlag, UUID packetNumber) {
        this(sender, variable, packet_type, isErrorFlag, "", packetNumber);
    }

    public Packet(UUID sender, Variable variable, PACKET_TYPE packet_type, Boolean isErrorFlag, String data) {
        this(sender, variable, packet_type, isErrorFlag, data, UUID.randomUUID());
    }

    public Packet(UUID sender, Variable variable, PACKET_TYPE packet_type, Boolean isErrorFlag) {
        this(sender, variable, packet_type, isErrorFlag, "", UUID.randomUUID());
    }


    public Packet(String packetString){
        isValid = true;
        String [] packetInfos = packetString.split("\\|");
        try{
            this.packet_type = PACKET_TYPE.valueOf(packetInfos[0]);
            this.isErrorFlag = Boolean.valueOf(packetInfos[1]);
        }catch (Exception e){
            isValid = false;
            error_type = SERVER_ERROR.WRONG_PACKET_TYPE;
            System.err.println("Erreur : Le paquet n'est pas bien formé : "+SERVER_ERROR.WRONG_PACKET_TYPE);
        }

        try{
            this.sender = UUID.fromString(packetInfos[2]);
        }catch (Exception e){
            isValid = false;
            error_type = SERVER_ERROR.NOT_AN_UUID;
            System.err.println("Erreur : Le paquet n'est pas bien formé : "+SERVER_ERROR.NOT_AN_UUID);
        }

        this.variable = new Variable(packetInfos[3]);
        if( this.variable.getAddress().getOffset() == null || variable.getAddress().getNodeId() == null){
            error_type = SERVER_ERROR.MALFORMED_ADR;
            isValid = false;
        }
        if(this.variable.getName() == null){
            error_type = SERVER_ERROR.MALFORMED_VARIABLE;
            isValid = false;
        }

        try{
            this.packetNumber = UUID.fromString(packetInfos[4]);
        }catch (Exception e){
            isValid = false;
            error_type = SERVER_ERROR.WRONG_PACKET_NUMBER;
            System.err.println("Erreur : Le paquet n'est pas bien formé : "+SERVER_ERROR.WRONG_PACKET_NUMBER);
        }
        if(packetInfos.length == 5) {
            this.data = "";
        } else {
            this.data = packetInfos[5];
        }


    }

    public void setData(String data){this.data = data;}

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(packet_type);
        sb.append("|");
        sb.append(isErrorFlag);
        sb.append("|");
        sb.append(sender);
        sb.append("|");
        sb.append(variable);
        sb.append("|");
        sb.append(packetNumber);
        sb.append("|");
        sb.append(data);
        return sb.toString();
    }

    public Boolean isValid(List<UUID> knownNodes, UUID id, Integer memorySize){
        if( !this.packet_type.equals(PACKET_TYPE.ALLOCATE) && !id.equals(this.variable.getAddress().getNodeId()) && !knownNodes.contains(this.variable.getAddress().getNodeId()) ){
            isValid = false;
            error_type = SERVER_ERROR.UNKNOWN_NODE_ID;
        }
        if( memorySize <= this.variable.getAddress().getOffset()){
            isValid = false;
            error_type = SERVER_ERROR.OFFSET_OUT_OF_BOUND;
        }
        return isValid;
    }

    public Packet getCreationErrorPacket(UUID idSender){
        return new Packet(idSender,this.variable,this.packet_type,true,this.error_type.toString());
    }

    public Packet getNonAllocatedAdrErrorPacket(UUID idSender){
        Packet errorPacket = new Packet(idSender,this.variable,this.packet_type,true);
        errorPacket.error_type = SERVER_ERROR.ADR_NOT_ALLOCATED;
        return errorPacket;
    }

    public Packet getCouldntFindEnoughSpaceErrorPacket(UUID idSender){
        Packet errorPacket = new Packet(idSender,this.variable,this.packet_type,true);
        errorPacket.data = SERVER_ERROR.COULDNT_FIND_ENOUGH_SPACE.toString();
        return errorPacket;
    }

    public Packet getResponsePacket(UUID senderID, String data){
        return new Packet(senderID,this.variable,this.packet_type,false,data,this.packetNumber);
    }


}
