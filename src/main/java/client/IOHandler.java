package client;

import server.ServerException;
import server.Variable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeoutException;

public class IOHandler extends Thread{

    private String inputString;
    private final PropertyChangeListener pcl;
    private final Client client;
    private Boolean isRunning;

    public IOHandler(Client client){
        this.client = client;
        pcl = evt -> handleInput(inputString);

        // Lance le Thread en appellant run()
        this.start();
    }


    public void run() {
        isRunning = true;
        printWelcomeMessage();
        while(isRunning){
            Scanner scanner = new Scanner(System.in);
            String inputString = scanner.nextLine();
            this.inputString = inputString;

            pcl.propertyChange(new PropertyChangeEvent(this,"inputString",inputString,inputString));
        }
    }



    private void handleInput(String inputString) {
        if( inputString.isEmpty() ){
            System.err.println("Entrez une instruction valide");

        }else if( inputString.charAt(0) == '/'){

            try {
                handleCommand(inputString);
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
            }

        }else if(client.isConnected) {

            try {
                handleStatement(inputString);
            } catch (ServerException e) {
                System.err.println("Erreur de communication avec le serveur");
                e.printStackTrace();
            } catch (IOException e){
                e.printStackTrace();
            }

        }else{
            System.err.println("Aucune connexion au server détectée");
        }
    }

    private void handleCommand(String command) throws IOException, TimeoutException {

        switch (command){
            case "/leave":
            case "/quit":
            case "/exit":
                handleExit();
                break;

            case "/variables":
                printVariables();
                break;

            case "/help":
                printHelp();
                break;

            default:
                System.err.println("Erreur : Commande non reconnue");
                break;

        }

    }
    private void printVariables(){
        if(this.client.variables.size() == 0){
            printGreen("Aucune variable n'est allouée pour le moment");
        }else{
            printGreen("Voici la liste des variables allouées :");
            for(Variable var : this.client.variables){
                if( var.getSize() > 1 ){
                    printGreen(var.getName() + "[]("+var.getSize()+")");
                }else{
                    printGreen(var.getName());
                }
            }
        }

    }

    private void handleExit() throws IOException, TimeoutException {
        printGreen("Fin de connexion et destruction des objets RabbitMQ.");
        client.terminate();
        isRunning = false;
    }

    private void handleStatement(String statement) throws ServerException, IOException {
        String callRegex = "^[a-z]+\\s*\\(\\s*[a-z]+([0-9]*[a-zA-Z]*)*\\s*(,\\s*[0-9]+\\s*){0,2}\\)$";
        if( !statement.matches(callRegex) ){
            printUnknownStatement();
            return;
        }

        // Signatures :
        // allocate(name,size)
        // read(name) , read(name,indice)
        // write(name,value) , write(name, indice, value)
        // free(name)
        String [] statementInfos = statement.split("\\(");
        String function = statementInfos[0].toLowerCase().trim();
        String params = statementInfos[1].substring(0, statementInfos[1].length()-1);

        handleVariableStatement(function,params);


    }

    private void handleVariableStatement(String function, String paramString) throws ServerException, IOException {
        String [] params = paramString.split(",");
        switch ( function ){
            case "read":
                if( params.length < 1){
                    System.out.println("length trop petite");
                    printUnknownStatement();
                }else if( params.length == 1 ){
                    Integer readValue = client.read( params[0].trim() );
                    if(readValue != null){
                        printBlue(readValue.toString());
                    }
                }else if( params.length == 2){
                    Integer readValue = client.read( params[0].trim() , Integer.parseInt(params[1].trim()) );
                    if(readValue != null){
                        printBlue(readValue.toString());
                    }
                }else{
                    System.out.println("length trop grande");
                    printUnknownStatement();
                }
                break;

            case "write":
                if( params.length < 2){
                    printUnknownStatement();
                }else if( params.length == 2 ){
                    client.write(params[0].trim(),Integer.parseInt(params[1].trim()));
                }else if( params.length == 3){
                    client.write( params[0].trim() , Integer.parseInt(params[1].trim()) , Integer.parseInt(params[2].trim()) );
                }else{
                    printUnknownStatement();
                }
                break;

            case "allocate":

                if( params.length < 2){
                    printUnknownStatement();
                }else if( params.length == 2 ){
                    client.allocate(params[0].trim(),Integer.parseInt(params[1].trim()));
                }else{
                    printUnknownStatement();
                }
                break;

            case "free":
                if( params.length < 1){
                    printUnknownStatement();
                }else if( params.length == 1 ){
                    client.free(params[0].trim());
                }else{
                    printUnknownStatement();
                }
                break;

            default:
                printUnknownStatement();
                break;
        }
    }

    private void printWelcomeMessage(){
        printGreen("Bienvenue dans l'interface interactive de nahavi.");
        printHelp();
        printInstructions();
    }

    private void printUnknownStatement(){
        System.err.println("Instruction non reconnue !");
        printInstructions();
    }

    private void printInstructions(){
        printGreen("Utilisez les instructions suivantes : ");
        printGreen(" - allocate(name,size) ");
        printGreen(" - free(name) ");
        printGreen(" - write(name,value) ou write(name,index,value) ");
        printGreen(" - read(name) ou read(name,index) ");
        printGreen("");
        printGreen("Par exemple : allocate(toto,1)");

    }

    private void printHelp(){
        printGreen("Voici la liste des commandes disponible :");
        printGreen(" - \"/exit , /quit, /leave\" : Quitte le service et termine le programme.");
        printGreen(" - \"/variables\" : Affiche toutes les variables déclarés.");
        printGreen(" - \"/help\" : Affiche ce menu d'aide.");
        printGreen("");
    }

    public void printGreen(String strToPrint){
        System.out.println("\033[32m"+strToPrint+"\033[0m");
    }
    public void printBlue(String strToPrint){
        System.out.println("\033[32m"+strToPrint+"\033[0m");
    }

}
