package client;

import com.rabbitmq.client.*;
import server.*;
import server.Address;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Client {
    private final UUID id;
    private Connection connection;
    private Channel channel;
    private final String responseQueue;
    private String myListeningNode; /* Actually, his queue */
    private UUID waitingForResponse;
    public List<Variable> variables;
    private final Lock lock;
    private final Condition blockingPrimitive;

    private Boolean isUnlockedByForce;
    Boolean isConnected;
    private Boolean isValueReturned;
    private Boolean requestValidated;
    private final Boolean isInteractiveMode;
    private Integer returnedValue;
    private String exceptionMessage;

    private static final String EXTERNAL_EXCHANGE_NAME = "client_director";

    public Client(Boolean isInteractiveMode) throws Exception {
        this.id = UUID.randomUUID();
        System.out.println("Voici mon identifiant de client : "+id.toString());
        this.isInteractiveMode = isInteractiveMode;
        isConnected = false;
        this.responseQueue = "clientResponse@"+id.toString();
        this.variables = new ArrayList<>();
        lock = new ReentrantLock();
        blockingPrimitive = lock.newCondition();
        initConnection();
        channel.queueDeclare(responseQueue,true,false,false,null);
        System.out.println("Demande de connexion...");
        channel.exchangeDeclare(EXTERNAL_EXCHANGE_NAME, "direct");
        channel.queueBind(responseQueue, EXTERNAL_EXCHANGE_NAME, "client");
        initResponseQueue();
        channel.basicPublish(EXTERNAL_EXCHANGE_NAME, "connection", null, id.toString().getBytes(StandardCharsets.UTF_8));
        waitForResponse();

    }

    private void initResponseQueue() throws IOException {
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String receivedPacketString = new String(delivery.getBody(), StandardCharsets.UTF_8);
            Packet receivedPacket = new Packet(receivedPacketString);
            /* On vérifie que le message n'est pas un message d'erreur */
            if(receivedPacket.isErrorFlag) {
                /* Si c'en est un : on débloque la primitive bloqué en passant à vrai le booleen isUnlockedByForce, ce qui déclenchera une exception */
                exceptionMessage = "Le serveur a renvoyé une erreur de type : " + receivedPacket.error_type;
                notifyErrorReceived();
                return;
            }
            /* On gère le message en fonction de son type */
            switch (receivedPacket.packet_type){
                case SETCONNECTION:
                    handleSetConnectionMessage(receivedPacket);
                    break;

                case READ:
                    handleReadMessage(receivedPacket);
                    break;

                case WRITE:
                    handleWriteMessage(receivedPacket);
                    break;

                case FREE:
                    handleFreeMessage(receivedPacket);
                    break;

                case ALLOCATE:
                    handleAllocateMessage(receivedPacket);
                    break;

                default:
                    System.err.println("J'ai reçu une requête a été reçu mais je ne sais pas quoi en faire.");
                    break;
            }
        };
        channel.basicConsume(responseQueue, true, deliverCallback, consumerTag -> {
        });
    }

    private void initConnection() throws Exception{
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }

    private void handleSetConnectionMessage(Packet receivedPacket) {
        String [] infos = receivedPacket.data.split(":");
        myListeningNode = infos[0];
        if(infos.length == 2) {
            String [] receivedVariables = infos[1].split("/");
            for(String variableString : receivedVariables){
                Variable currentVar = new Variable(variableString);
                variables.add(currentVar);
            }
        }
        isConnected = true;
        if(isInteractiveMode){
            new IOHandler(this);
        }
        notifyResponseReceived();
        System.out.println("Je suis connecté à la queue : " + myListeningNode);
    }

    private void handleReadMessage(Packet receivedPacket) {
        /* On vérifie que le paquet reçu est bien celui que nous attendions */
        if(!waitingForResponse.equals(receivedPacket.packetNumber)) {
            System.out.println("Erreur : réponse inattendu");
        }
        /* Dans ce cas, on récupère ce qui nous interesse dans les data, on affecte une valeur aléatoire à waitingForResponse puis on débloque la primitive en cours */
        returnedValue = Integer.parseInt(receivedPacket.data);
        waitingForResponse = UUID.randomUUID();
        notifyResponseReceived();
    }

    private void handleWriteMessage(Packet receivedPacket) {
        /* On vérifie que le paquet reçu est bien celui que nous attendions */
        if(!waitingForResponse.equals(receivedPacket.packetNumber)) {
            System.err.println("Erreur : réponse inattendu");
        }
        /* Dans ce cas, on récupère ce qui nous interesse dans les data, on affecte une valeur aléatoire à waitingForResponse puis on débloque la primitive en cours */
        waitingForResponse = UUID.randomUUID();
        requestValidated = Boolean.getBoolean(receivedPacket.data);
        notifyResponseReceived();
    }

    private void handleFreeMessage(Packet receivedPacket) {
        /* On retire la variable correspondante de notre liste de variable */
        if( this.variables.contains(receivedPacket.variable) ) {
            removeVariable(receivedPacket.variable);
        }
        /* Si la requète free venait de nous, alors on récupère ce qui nous interesse dans les data, on affecte une valeur aléatoire à waitingForResponse puis on débloque la primitive en cours */
        if(waitingForResponse.equals(receivedPacket.packetNumber)) {
            waitingForResponse = UUID.randomUUID();
            requestValidated = Boolean.getBoolean(receivedPacket.data);
            notifyResponseReceived();
        }
    }

    private void handleAllocateMessage(Packet receivedPacket) {
        /* On ajoute la variable correspondante à notre liste de variable */
        if( !this.variables.contains(receivedPacket.variable) ) {
            this.variables.add(receivedPacket.variable);
        }
        /* Si la requète allocate venait de nous, alors on récupère ce qui nous interesse dans les data, on affecte une valeur aléatoire à waitingForResponse puis on débloque la primitive en cours */
        if( waitingForResponse != null && waitingForResponse.equals(receivedPacket.packetNumber)) {
            waitingForResponse = UUID.randomUUID();
            requestValidated = Boolean.getBoolean(receivedPacket.data);
            notifyResponseReceived();
        }
    }

    public Integer read(String varName) throws ServerException, IOException {
        Variable var = getVariable(varName);
        if(var == null) {
            System.err.println("Erreur : La variable \"" + varName + "\" n'existe pas.");
            return null;
        }
        /* On envoie la requète au node qui nous écoute */
        UUID requestId = UUID.randomUUID();
        waitingForResponse = requestId;
        Packet read = new Packet(this.id, var, PACKET_TYPE.READ, false, requestId);
        channel.basicPublish("", myListeningNode, null, read.toString().getBytes(StandardCharsets.UTF_8));
        /* Puis on se bloque jusqu'à ce que la réponse arrive */
        waitForResponse();
        return returnedValue;
    }

    public Integer read(String varName, int index) throws ServerException, IOException {
        Variable var = getVariable(varName);
        if(var == null) {
            System.err.println("Erreur : La variable \"" + varName + "\" n'existe pas.");
            return null;
        }
        /* On envoie la requète au node qui nous écoute */
        UUID requestId = UUID.randomUUID();
        waitingForResponse = requestId;
        var = var.atIndex(index);
        Packet read = new Packet(this.id, var, PACKET_TYPE.READ, false, requestId);
        channel.basicPublish("", myListeningNode, null, read.toString().getBytes(StandardCharsets.UTF_8));
        /* Puis on se bloque jusqu'à ce que la réponse arrive */
        waitForResponse();
        return returnedValue;
    }

    public Boolean write(String variableName, Integer value) throws IOException, ServerException {
        Variable var = getVariable(variableName);
        if( var == null ){
            System.err.println("Erreur : La variable \"" + variableName + "\" n'existe pas.");
            return false;
        }

        /* On envoie la requète au node qui nous écoute */
        UUID requestId = UUID.randomUUID();
        waitingForResponse = requestId;
        Packet write = new Packet(this.id, var, PACKET_TYPE.WRITE, false, value.toString(), requestId);
        channel.basicPublish("", myListeningNode, null, write.toString().getBytes(StandardCharsets.UTF_8));
        /* Puis on se bloque jusqu'à ce que la réponse arrive */
        waitForResponse();
        return requestValidated;
    }

    public Boolean write(String variableName, Integer index, Integer value) throws IOException, ServerException {
        Variable var = getVariable(variableName);
        if( var == null ){
            System.err.println("Erreur : La variable \"" + variableName + "\" n'existe pas.");
            return false;
        }

        /* On envoie la requète au node qui nous écoute */
        UUID requestId = UUID.randomUUID();
        waitingForResponse = requestId;
        var = var.atIndex(index);
        Packet write = new Packet(this.id, var, PACKET_TYPE.WRITE, false, value.toString(), requestId);
        channel.basicPublish("", myListeningNode, null, write.toString().getBytes(StandardCharsets.UTF_8));
        /* Puis on se bloque jusqu'à ce que la réponse arrive */
        waitForResponse();
        return requestValidated;
    }

    public Boolean free(String variableName) throws IOException, ServerException {
        Variable var = getVariable(variableName);
        if( var == null || !this.variables.contains(var) ) {
            System.err.println("Erreur : La variable \"" + variableName + "\" n'existe pas.");
            return false;
        }
        /* On envoie la requète au node qui nous écoute */
        UUID requestId = UUID.randomUUID();
        waitingForResponse = requestId;
        Packet free = new Packet(this.id, var, PACKET_TYPE.FREE, false, requestId);
        channel.basicPublish("", myListeningNode, null, free.toString().getBytes(StandardCharsets.UTF_8));
        /* Puis on se bloque jusqu'à ce que la réponse arrive */
        waitForResponse();
        return requestValidated;
    }

    public Boolean allocate(String name, Integer size) throws IOException, ServerException {
        for( Variable v : this.variables) {
            if(name.equals(v.getName())) {
                System.err.println("Erreur : La variable \"" + name + "\" existe déjà.");
                return false;
            }
        }
        /* On envoie la requète au node qui nous écoute */
        UUID requestId = UUID.randomUUID();
        Packet allocate = new Packet(this.id, new Variable(name, new Address(UUID.randomUUID(),0), size), PACKET_TYPE.ALLOCATE, false, requestId);
        waitingForResponse = requestId;
        channel.basicPublish("", myListeningNode, null, allocate.toString().getBytes(StandardCharsets.UTF_8));
        /* Puis on se bloque jusqu'à ce que la réponse arrive */
        waitForResponse();
        return requestValidated;
    }

    private void waitForResponse() throws ServerException {
        isValueReturned = false;
        isUnlockedByForce = false;
        lock.lock();

        try {
            while (!isValueReturned && !isUnlockedByForce) {
                blockingPrimitive.await();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            lock.unlock();
        }
        /* Si la méthode a été débloqué de force on renvoie l'exception correspondante */
        if(isUnlockedByForce) {
            throw new ServerException(exceptionMessage);
        }
    }

    private void notifyResponseReceived(){
        lock.lock();
        try {
            isValueReturned = true;
            blockingPrimitive.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private void notifyErrorReceived(){
        lock.lock();
        try {
            isUnlockedByForce = true;
            blockingPrimitive.signalAll();
        } finally {
            lock.unlock();
        }
    }

    private Variable getVariable(String varName){
        for( Variable var : variables){
            if( var.getName().equals(varName) ){
                return var;
            }
        }
        return null;
    }

    private boolean removeVariable(Variable var) {
        for(Variable v : this.variables) {
            if(var.equals(v)) {
                this.variables.remove(v);
                return true;
            }
        }
        return false;
    }

    public void terminate() throws IOException, TimeoutException {
        Packet disconnectionPacket = new Packet(id,new Variable("",new Address(UUID.randomUUID(),0),1),PACKET_TYPE.CLIENTDISCONNECTION,false);
        channel.basicPublish("", myListeningNode, null, disconnectionPacket.toString().getBytes(StandardCharsets.UTF_8));
        channel.queueUnbind(responseQueue,EXTERNAL_EXCHANGE_NAME,"client");
        channel.queueDelete(responseQueue);
        channel.close();
        connection.close();
    }


    public static void main(String[] args) throws Exception {
        new Client(true);
    }
}
